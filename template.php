<?php
/**
 * Process function for navigation pane.
 */
function versatile_process_pane_navigation(&$vars) {
  $vars['main_menu'] = theme('links__system_main_menu', array(
    'links' => menu_main_menu(),
    'attributes' => array('id' => 'main-menu', 'class' => array('clearfix')),
  ));
  $vars['secondary_menu'] = theme('links__system_secondary_menu', array(
    'links' => menu_secondary_menu(),
    'attributes' => array('id' => 'secondary-menu', 'class' => array('clearfix')),
  ));
}

/**
 * Preprocess function for site template layout.
 */
function versatile_preprocess_versatile_site_template(&$variables) {
  $variables['header_outside'] = theme_get_setting('versatile_header_outside');
  $variables['footer_outside'] = theme_get_setting('versatile_footer_outside');
}