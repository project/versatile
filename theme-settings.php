<?php
/**
 * Implements hook_form_FORM_ID_alter().
 */
function versatile_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['versatile_header_outside'] = array(
    '#type' => 'checkbox',
    '#title' => t('Place header outside page wrapper'),
    '#default_value' => theme_get_setting('versatile_header_outside'),
    '#description' => t('Makes the header element be placed outside of the page wrapper.'),
  );

  $form['versatile_footer_outside'] = array(
    '#type' => 'checkbox',
    '#title' => t('Place footer outside page wrapper'),
    '#default_value' => theme_get_setting('versatile_footer_outside'),
    '#description' => t('Makes the footer element be placed outside of the page wrapper.'),
  );
}

function versatile_preprocess_panels_pane(&$vars) {
  drupal_set_message(print_r($vars, TRUE));
}