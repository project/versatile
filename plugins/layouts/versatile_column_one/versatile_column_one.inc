<?php
/**
 * @file
 * Plugin definition for Versatile one column layout.
 */

$plugin = array(
  'title' => t('Versatile one column'),
  'theme' => 'versatile_column_one',
  'icon' => 'versatile-column-one.png',
  'category' => 'Versatile',
  'regions' => array(
    'main' => t('Main'),
  ),
);