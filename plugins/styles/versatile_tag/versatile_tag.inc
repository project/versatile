<?php
/**
 * @file
 * Definition of the Versatile tag panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Versatile Tag'),
  'description' => t('Wrap the content of a pane in a HTML element.'),
  'render pane' => 'versatile_tag_style_render_pane',
  'render region' => 'versatile_tag_style_render_region',
  'pane settings form' => 'versatile_tag_style_settings_form',
  'settings form' => 'versatile_tag_style_settings_form',
);

/**
 * Render callback.
 */
function theme_versatile_tag_style_render_pane($vars) {
  $content = $vars['content'];
  $settings = $vars['settings'];

  if (empty($settings['tag'])) {
    $settings['tag'] = 'div';
  }

  if (empty($settings['class'])) {
    $settings['class'] = '';
  }

  $output = '<' . $settings['tag'];
  $output .= !empty($settings['class']) ? ' class="' . $settings['class'] . '">' : '>';
  $output .= $content->content . '</' . $settings['tag'] . '>';
  
  return $output;
}

/**
 * Render callback.
 */
function theme_versatile_tag_style_render_region($vars) {
  $settings = $vars['settings'];
  $panes = $vars['panes'];
  
  if (empty($settings['tag'])) {
    $settings['tag'] = 'div';
  }

  if (empty($settings['class'])) {
    $settings['class'] = '';
  }

  $output = '<' . $settings['tag'];
  $output .= !empty($settings['class']) ? ' class="' . $settings['class'] . '">' : '>';
  $output .= implode("\n", $panes) . '</' . $settings['tag'] . '>';

  return $output;
}

/**
 * Settings form callback.
 */
function versatile_tag_style_settings_form($style_settings) {
  $form['tag'] = array(
    '#type' => 'select',
    '#title' => t('HTML element'),
    '#options' => array(
      'div' => 'DIV',
      'span' => 'SPAN',
      'h1' => 'H1',
      'h2' => 'H2',
      'h3' => 'H3',
      'h4' => 'H4',
      'h5' => 'H5',
      'h6' => 'H6',
      'p' => 'P',
      'pre' => 'PRE',
      'address' => 'ADDRESS',
    ),
    '#default_value' => (isset($style_settings['tag'])) ? $style_settings['tag'] : 'div',
  );
  $form['class'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS class'),
    '#default_value' => (isset($style_settings['class'])) ? $style_settings['class'] : '',
  );

  return $form;
}

